package com.nicoleapp.trafficlight;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by user on 26/10/18.
 */

public class LightsFragment extends Fragment {
    private static final long START_TIME_IN_MILLIS = 17000;
    public static boolean isAppInFg = false;
    public static boolean isScrInFg = false;
    public static boolean isChangeScrFg = false;
    private TextView textView;
    private TextView textView2;
    private RedSquareView mredSquareView;
    private YellowSquareView myellowSquareView;
    private GreenSquareView mgreenView;
    private int i;


    private CountDownTimer mCountDownTimer;

    private boolean mTimerRunning;

    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;
    private String mParam1;


    private EventBus bus = EventBus.getDefault();

    private int count;
    private String callback;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Confirm this fragment has menu items.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //mcustomView = (CustomView) findViewById(R.id.customView);


        return inflater.inflate(R.layout.fragment_screen_1, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mredSquareView= (RedSquareView) view.findViewById(R.id.customView);
        myellowSquareView= (YellowSquareView) view.findViewById(R.id.redSquare);
        mgreenView = (GreenSquareView) view.findViewById(R.id.greeSquare);


        callback = "Red";
        count = 0;


    }


    @Override
    public void onStart() {


        if (mTimerRunning) {
            pauseTimer();
        } else {
            startTimer();

        }


        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();


        if (!isScrInFg || !isChangeScrFg) {
            isAppInFg = false;
            pauseTimer();


        }
        isScrInFg = false;
        mredSquareView.swapBack();

    }




    public void startTimer() {
        if(count == 16){
            count = 0;
        }
        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                if(count  == 17){
                    count = 0;
                }

                mTimeLeftInMillis = millisUntilFinished;
                if(count < 9 ) {
                    mredSquareView.swapColor();
                    myellowSquareView.swapBack();
                    mgreenView.swapBack();

                }else if(count > 8 && count < 15){
                    mgreenView.swapColor();
                    mredSquareView.swapBack();
                    myellowSquareView.swapBack();

                }else if(count > 14 && count <= 16){
                    myellowSquareView.swapColor();
                    mgreenView.swapBack();
                    mredSquareView.swapBack();

                }
                if(count == 0){
                    bus.post(new Communication(callback + " to Red"));
                    callback = "Red";
                }else if(count == 9){
                    bus.post(new Communication("Red to Green"));
                    callback = "Green";
                }else if(count == 15){
                    bus.post(new Communication("Green to Yellow"));
                    callback = "Yellow";
                }
                count = count +1;


            }

            @Override
            public void onFinish() {

                mTimeLeftInMillis = START_TIME_IN_MILLIS;
                if(count  == 17){
                    count = 0;
                }
                startTimer();
                mredSquareView.swapBack();
                mTimerRunning = false;

            }

        }.start();



        mTimerRunning = true;

    }

    private void pauseTimer() {
        mCountDownTimer.cancel();
        mTimerRunning = false;

    }

    private void updateCountDownText() {


    }

    public void resetTime(){
        pauseTimer();
        startTimer();

        updateCountDownText();
    }



     @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the fragment menu items.
        inflater.inflate(R.menu.fragment_menu_items, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if(itemId == R.id.menuAbout)
        {   count = 0;

            resetTime();

        }


        return super.onOptionsItemSelected(item);
    }


}
