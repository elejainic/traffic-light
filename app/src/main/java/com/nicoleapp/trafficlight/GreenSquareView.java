package com.nicoleapp.trafficlight;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by user on 25/10/18.
 */

public class GreenSquareView extends View {

    private static final int SQUARE_SIZE = 200;
    private Rect mRectSquare;
    private Paint mPainSquare;


    public GreenSquareView(Context context) {
        super(context);

        init(null);
    }

    public GreenSquareView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(null);
    }

    public GreenSquareView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(null);
    }

    public GreenSquareView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(null);
    }

    private void init(@Nullable AttributeSet set){

        mRectSquare = new Rect();
        mPainSquare = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPainSquare.setColor(Color.parseColor("#085B19"));

    }

    public void swapColor(){
        mPainSquare.setColor(Color.parseColor("#0AF339"));

        postInvalidate();
    }
    public void swapBack(){
        mPainSquare.setColor(Color.parseColor("#085B19"));

        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int width = this.getMeasuredWidth();

        mRectSquare.left = (width / 2) - 100;
        mRectSquare.top = 50;
        mRectSquare.right = mRectSquare.left + SQUARE_SIZE;
        mRectSquare.bottom = mRectSquare.top + SQUARE_SIZE;

        canvas.drawRect(mRectSquare, mPainSquare);

    }
}
