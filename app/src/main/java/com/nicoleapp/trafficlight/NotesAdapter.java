package com.nicoleapp.trafficlight;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nicoleapp.trafficlight.database.DatabaseHelper;
import com.nicoleapp.trafficlight.database.model.Note;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {

    private Context context;
    private List<Note> notesList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView log;
        public TextView dot;
        public TextView timestamp;

        public MyViewHolder(View view) {
            super(view);
            log = view.findViewById(R.id.note);
            dot = view.findViewById(R.id.dot);
            timestamp = view.findViewById(R.id.timestamp);

        }
    }


    public NotesAdapter(Context context, List<Note> notesList) {
        this.context = context;
        this.notesList = notesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.log_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Note note = notesList.get(position);

        holder.log.setText(note.getNote());

        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));

        // Formatting and displaying timestamp
        holder.timestamp.setText(formatDate(note.getTimestamp()));
    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }

    /**
     * Formatting timestamp to `MMM d` format
     * Input: 2018-02-21 00:15:42
     * Output: Feb 21
     */
    private String formatDate(String dateStr) {
        Date date = null;
        String formattedTime = "";
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr);
            formattedTime = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa").format(date);

            return formattedTime;
        } catch (ParseException e) {

        }

        return "";
    }

    public void deleteAllNotes()
    {
        DatabaseHelper db = new DatabaseHelper(context);
        db.deleteAll();
        notesList.clear(); //clear list
        notifyDataSetChanged();




    }
}