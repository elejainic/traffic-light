package com.nicoleapp.trafficlight;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nicoleapp.trafficlight.database.DatabaseHelper;
import com.nicoleapp.trafficlight.database.model.Note;
import com.nicoleapp.trafficlight.utils.MyDividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 26/10/18.
 */

public class LogFragment extends Fragment {
    private static final long START_TIME_IN_MILLIS = 16000;


    private NotesAdapter mAdapter;
    private List<Note> notesList = new ArrayList<>();
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;

    private DatabaseHelper db;
    NotesAdapter notesAdapter;
    private EventBus bus = EventBus.getDefault();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Confirm this fragment has menu items.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //mcustomView = (CustomView) findViewById(R.id.customView);


        View rootView=inflater.inflate(R.layout.fragment_screen_2, null);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        db = new DatabaseHelper(getActivity().getApplicationContext());



        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);

        mAdapter = new NotesAdapter(getActivity().getApplicationContext(), notesList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);



    }

    @Override
    public void onStart() {
        super.onStart();

        createNote("App in the Foreground");
    }

    @Override
    public void onStop() {
        super.onStop();

        createNote("App in the Background");

    }

    @Override
    public  void onResume(){
        super.onResume();

        bus.register(this);

    }
    @Override
    public void onPause(){
        super.onPause();

        bus.unregister(this);
    }


    private void createNote(String note) {
        // inserting note in db and getting
        // newly inserted note id
        long id = db.insertNote(note);

        // get the newly inserted note from db
        Note n = db.getNote(id);

        if (n != null) {
            // adding new note to array list at 0 position
            notesList.add(0, n);

            // refreshing the list
            mAdapter.notifyDataSetChanged();


        }
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the fragment menu items.
        inflater.inflate(R.menu.fragment_menu_log, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        boolean showMessage = false;

        String message = "You click fragment ";

        if(itemId == R.id.menuAbout)
        {
            mAdapter.deleteAllNotes();



        }


        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public  void communicate (Communication c){
        createNote(c.getNombre());
    }

}
